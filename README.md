# INFO

## Author: Marcin

###### Description: I am creating browser action game for my degree project. I am using Phaser3 with Parcel bundler.

###### Game controls: WSAD - movement | Spacebar - Melee attack | B - Range attack

###### Technologies:

- HTML5 & JavaScript(ES6)
- Phaser 3
- Parcel

###### Sources:

- https://phaser.io/ - engine
- https://parceljs.org/ - bundler
- https://github.com/ourcade/phaser3-parcel-template - ready template of the project (engine + configured bundler)
- https://www.dafont.com/bitmap.php - free fonts (not using right now, located in assets/fonts folder)
- http://labs.phaser.io/assets/fonts/bitmap/ - free phaser fonts (chiller.xml)
- https://0x72.itch.io/dungeontileset-ii - free assets for game (licensed CC-0)
- https://rvros.itch.io/animated-pixel-hero - free assets of hero for game

###### Installation and first run:

- Install Node > v.14.15.4 for npm manger
- Download and unpack project
- Navigate to the unpacked catalog via CMD (cd myproject)
- Inside catalog 'myproject' run command 'npm i'
- Next command 'npm start'
- Project should be available under url http://localhost:8000

##### Structure:

- dist - distribution folder genereated by the bundler, ready for production code goes there
- INFO - author main documents, notes, tips, diagrams and other ideas to keep work in correct order
- public - folder configured for parcel bundler, all assets goes there in order to be correct loaded
- src - source code, all scenes, objects, classes etc.

##### Additional:

- No additional info for now
