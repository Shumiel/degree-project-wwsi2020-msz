import Fog from "./entities/objects/Fog.js";

/*
  mapCreate is responsible for creating map in the main scene
*/
export function mapCreate(){
    //create world bounds
    this.matter.world.setBounds(16, 16, 610, 316);

    //add fog to the map
    const fog = new Fog({x: 320, y: 160, labelName: 'whiteFog', scene: this, particleSpeed: 90, particleAlpha: 50, particleLifespan: 80, texture: 'magicfog', size: 5});

    //basic map set - tilemap approach
    const map = this.make.tilemap({key:'map'});
    const tilest = map.addTilesetImage('0x72_DungeonTilesetII_v1.3', 'tiles', 16, 16, 0 ,0);
    //layers - order is important, due to Tiled software we have 3 layers, they should be loaded in correct order 3>1>2>4>5
    const layer3 = map.createStaticLayer('Tile Layer 3', tilest, 0, 0); //layer3
    const layer1 = map.createStaticLayer('Tile Layer 1', tilest, 0, 0); //layer1
    const layer2 = map.createStaticLayer('Tile Layer 2', tilest, 0, 0); //layer2
    const layer4 = map.createStaticLayer('Tile Layer 4', tilest, 0, 0); //layer4
    const layer5 = map.createStaticLayer('Tile Layer 5', tilest, 0, 0); //layer5
    //layers collision set - turned off because I decided add collision manually with setBounds()
    layer1.setCollisionByProperty({collides: true});
    this.matter.world.convertTilemapLayer(layer1);
    layer2.setCollisionByProperty({collides: true});
    this.matter.world.convertTilemapLayer(layer2);
    layer3.setCollisionByProperty({collides: true});
    this.matter.world.convertTilemapLayer(layer3);
    layer4.setCollisionByProperty({collides: true});
    this.matter.world.convertTilemapLayer(layer4);
    //Phaser 3’s Tilemap API does not support tile animation
    ////console.log(tilest.tileData);
    //add missing map animation
    this.anims.create({
        key: 'totemlavaAnimation',
        frames: this.anims.generateFrameNumbers('totemlava', {start:0, end: 12, first: 0}),
        frameRate: 6,
        repeat: -1
      });
      for (let i = 88; i <= 552; i=i+80) {
        this.add.sprite(i,25,'totemlava').play('totemlavaAnimation');
          if(i == 248){
            i+=144;
            this.add.sprite(i,25,'totemlava').play('totemlavaAnimation');
          }               
        }
        this.anims.create({
            key: 'totemwaterAnimation',
            frames: this.anims.generateFrameNumbers('totemwater', {start:0, end: 12, first: 0}),
            frameRate: 6,
            repeat: -1
          });
        this.add.sprite(296,40,'totemwater').play('totemwaterAnimation');
        this.add.sprite(344,40,'totemwater').play('totemwaterAnimation');

    //add fog to the map
    const fog2 = new Fog({x: 320, y: 350, labelName: 'whiteFogAtGate', scene: this, particleSpeed: 30, particleAlpha: 50, particleLifespan: 30, texture: 'magicfog', size: 0.5});


}