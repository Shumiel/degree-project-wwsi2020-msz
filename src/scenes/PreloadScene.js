/*
    PreloadScene - loads all needed assets here, before game will be available
*/
export default class PreloadScene extends Phaser.Scene {
  constructor() {
    super("PreloadScene");
  }

  //stressTest - large load test, loading tester, it will hover preloadscene a little longer and show how it works
  stressTest() {
    for (let i = 0; i < 300; i++) {
      this.load.spritesheet("soul" + i, "./assets/soul.png", {
        frameHeight: 32,
        frameWidth: 32,
      });
    }
  }

  //display status of the load
  displayLoadingInfo() {
    const width = this.cameras.main.width;
    const height = this.cameras.main.height;
    //text 'Loading...'
    let loadingText = this.make.text({
      x: this.game.renderer.width / 2,
      y: this.game.renderer.height / 2,
      text: "Loading...",
      style: {
        font: "28px monospace",
        fill: "#ffffff",
      },
    });
    loadingText.setOrigin(0.5, 0.5);
    //progress bar
    let progressBar = this.add.graphics({
      fillStyle: {
        color: 0xffffff, //white
      },
    });
    //percent number
    let percentText = this.make.text({
      x: width / 2,
      y: height / 2 + 25,
      text: "0%",
      style: {
        font: "18px monospace",
        fill: "#ffffff",
      },
    });
    percentText.setOrigin(0.5, 0.5);
    //loading asset text
    let assetText = this.make.text({
      x: width / 2,
      y: height / 2 + 50,
      text: "",
      style: {
        font: "18px monospace",
        fill: "#ffffff",
      },
    });
    assetText.setOrigin(0.5, 0.5);
    //load events
    this.load.on(
      "progress",
      (percent) => {
        progressBar.fillRect(
          0,
          this.game.renderer.height / 2 + 70,
          this.game.renderer.width * percent,
          50
        );
        percentText.setText(parseInt(percent * 100) + "%");
        console.log(parseInt(percent * 100) + "%");
      },
      this
    );
    //log what file is loading
    this.load.on("fileprogress", function (file) {
      console.log(file.src);
      assetText.setText(file.src);
    });
    //on complete destroy elements of the scene
    this.load.on("complete", () => {
      console.log("Done! Let's play...");
      loadingText.destroy();
      progressBar.destroy();
      percentText.destroy();
      assetText.destroy();
    });
  }

  //mapPreload - loads needed files for mapCreation
  mapPreload() {
    this.load.image("tiles", "assets/map/0x72_DungeonTilesetII_v1.3.png");
    this.load.tilemapTiledJSON("map", "assets/map/map.json");
    this.load.spritesheet("totemlava", "./assets/map/totemlava.png", {
      frameWidth: 16,
      frameHeight: 16,
      startFrame: 0,
      endFrame: 2,
    }); //missing map animation
    this.load.spritesheet("totemwater", "./assets/map/totemwater.png", {
      frameWidth: 16,
      frameHeight: 16,
      startFrame: 0,
      endFrame: 2,
    }); //missing map animation
    this.load.image(
      "magicfog",
      "./assets/final/objects/magic/particles/white.png"
    );
  }

  playerPreload() {
    this.load.atlas(
      "hero",
      "./assets/ready/hero.png",
      "./assets/ready/hero_atlas.json"
    );
    this.load.animation("hero_anim", "./assets/ready/hero_anim.json");

    this.load.image("arrow", "./assets/final/arrow.png");
  }

  enemiesPreload() {
    //bigdemon
    this.load.atlas(
      "big_demon",
      "./assets/enemies/big_demon/big_demon.png",
      "./assets/enemies/big_demon/big_demon_atlas.json"
    );
    this.load.animation(
      "big_demon_anim",
      "./assets/enemies/big_demon/big_demon_anim.json"
    );
    //necromancer
    this.load.atlas(
      "necromancer",
      "./assets/enemies/necromancer/necromancer.png",
      "./assets/enemies/necromancer/necromancer_atlas.json"
    );
    this.load.animation(
      "necromancer_anim",
      "./assets/enemies/necromancer/necromancer_anim.json"
    );
    this.load.image(
      "necromancer_magic",
      "./assets/final/objects/magic/particles/necromancer.png"
    );
  }

  gatePreload() {
    this.load.spritesheet("gate", "./assets/gate/gate.png", {
      frameWidth: 32,
      frameHeight: 32,
    });
  }

  gameoverPreload() {
    this.load.spritesheet("skull", "./assets/Skull.png", {
      frameWidth: 160,
      frameHeight: 160,
    });
    this.load.bitmapFont(
      "game_font",
      "./assets/fonts/chiller.png",
      "./assets/fonts/chiller.xml"
    );
  }

  objectsPreload() {
    this.load.atlas(
      "coin",
      "./assets/final/objects/coin/coin.png",
      "./assets/final/objects/coin/coin_atlas.json"
    );
    this.load.animation(
      "coin_anim",
      "./assets/final/objects/coin/coin_anim.json"
    );
  }

  //phaser method - load all needed assets
  preload() {
    //add display logic for loader - show status
    this.displayLoadingInfo();

    // //loading tester
    //this.stressTest();

    //load map assets
    this.mapPreload();

    //load player assets
    this.playerPreload();

    //load enemies assets
    this.enemiesPreload();

    //load gate assets
    this.gatePreload();

    //load gameover assets
    this.gameoverPreload();
  }

  //phaser method called after preload is finished
  create() {
    this.scene.start("MainScene", { testFlag: "PreloadScene completed!" });
  }
}
