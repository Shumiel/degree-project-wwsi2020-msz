import Enemy from './Enemy.js';
import MagicProjectile from '../objects/MagicProjectile.js';
/**
 * Necromancer class that defines new behaviour for necromancer enemy, inherites basics from Enemy
 */
export default class Necromancer extends Enemy{
    /**
     * 
     * @param {object} data scene, x, y, texture, frame, fixedName, playerRef, gateRef 
     */
    constructor(data){        
        const {scene, x, y, texture, frame, fixedName, playerRef, gateRef, side} = data; //deconstruct received data
        // @ts-ignore
        super(scene.matter.world, x, y, texture, frame); //call super() in order to pass args to inherited Enemy>Phaser.Physics.Matter.Sprite
        this.scene.add.existing(this); //adds an existing Game Object to this Scene        
       
        /*START - Necromancer characteristic*/
        //Stats
        this.enemyName = "Necromancer";
        this.enemyFixedName = fixedName; // I use it in order to correctly load animations for specific enemy
        this.enemyLife = 55;
        this.attackPower = 25;
        this.movementspeed = 1; 
        //Flags
        this.isAttacking = false;  
        this.isAlive = true;
        this.didPlayerAgro = false; //flag for following logic, I use it in order to determine if enemy should follow player only
        this.side = side;
        //this.isTakingDamage = false;
        /* END  - Necromancer characteristic*/
        console.log(this.side);
        //reference to other objects
        this.playerRef = playerRef;
        this.gateRef = gateRef;   
        this.scene = scene;

        //without this assigment necromancer will appear in 0,0 (issues wiht inheritance in ES6, need to be investigated in future)
        this.x = x;
        this.y = y;
        
        //path properties - path is used to indicate necromancer way, he will follow it
        this.t = 0;
        this.duration = 10000;

        //attack config
        this.time = 0;



        this.hpRatio= 40/this.enemyLife;        

        this.create();
    }  

    /**
     * Phaser build-in method
     */
    create(){
        //create necromancer path, determine if this one go left or right side with this.side
        this.destinationForNecromancerToFollow(this.side);
    }

    /**
     * Phaser build-in method
     */
    preUpdate(time, delta){
        super.preUpdate(time, delta);

        if (this.t === -1)
        {
            return;
        }

        this.t += delta;

        if (this.t >= this.duration)
        {
            //  Reached the end
            this.setVelocity(0, 0);
        }
        else
        {
            let d = (this.t / this.duration);

            let p = this.curve.getPoint(d);

            this.setPosition(p.x, p.y);
        }

        this.necromancerAttack(this.playerRef, this.gateRef, delta);
        
    }

    /**
     * Necromancer attack method, call it in preUpdate() or update() in order to generate attacks comming from game object Necromancer(enemy)
     * Method determine if necromancer should shoot one of two objectives, gate or player - depends on the distance and player behavioure.
     * 
     * @param {object} playerRef Player class reference
     * @param {object} gateRef Gate class reference
     */
    necromancerAttack(playerRef, gateRef, delta){
        
        this.time += delta;

        if(!this.isAttacking && (this.time > 4000) && !this.isTakingDamage){
            //change flag in order to prevent every attack for each frame
            this.isAttacking = !this.isAttacking;
            //log info
            console.log('Necromancer attacked!');

            /*
            https://phaser.io/examples/v3/category/physics/matterjs

            https://phaser.io/examples/v3/view/physics/matterjs/attractors#
            
            https://phaser.io/examples/v3/view/physics/matterjs/particles-follow-body#
            */

            let magic = new MagicProjectile({x: this.x, y: this.y, texture: '', labelName: 'MagicProjectile', scene: this.scene, playerRef: this.playerRef, gateRef: this.gateRef, enemyRef: this});

            //change flag and let process to start new attack
            this.scene.time.delayedCall(4000, ()=> {
                this.isAttacking = !this.isAttacking;
            });
        }    
    }
            
    /**
     * Method is called in create() once, it determines if Necromancer should take path to the left area or right area.
     * 
     * @param {string} side 
     */
    destinationForNecromancerToFollow(side){
        //create graphic game object
        let graphics = this.scene.add.graphics();

        //set object to alpha equal 0 - it will dissapear
        graphics.setAlpha(0);
        //console.log(side);
        let line = new Phaser.Geom.Line();

        //configure line for our path
        if(side == 'right') {
            line = new Phaser.Geom.Line(320, 310, 590, 100);
        } else if (side == 'left'){
            line = new Phaser.Geom.Line(320, 310, 50, 100);
        }
        

        /*
            // alternative way with random points for our line, can be used in the future for more advanced path generations
            let line = new Phaser.Geom.Line(Phaser.Math.Between(100, 700), Phaser.Math.Between(100, 500), Phaser.Math.Between(100, 700), Phaser.Math.Between(100, 500));
        */
       
        /*
            // example default dots for created line, indicate end and start of the line
            graphics.fillStyle(0xff0000, 1);
            graphics.fillCircle(line.x1, line.y1, 8);
            graphics.fillStyle(0xff00ff, 1);
            graphics.fillCircle(line.x2, line.y2, 8);
        */

        //here we store points
        let points = [];
        //push point to the pool
        points.push(line.getPointA());
        //calculate length of the created line (path of the enemy), we use it to calculate waves of the line later
        const length = Phaser.Geom.Line.Length(line);
        //set waves
        const waves = 2;
        //// const waves = Math.ceil(length / 200);    //default waves
        
        //properties for waves drawing
        let vx = 0;
        let vy = 0;

        if(this.side === 'right'){
            vx = 80;
            vy = 50;
        } else if (this.side === 'left'){
            vx = -80;
            vy = -50;
        }        
        
        let prevX = line.x1;
        let prevY = line.y1;

        for (let i = 1; i <= waves; i++)
        {
            //Get a point on a line that's a given percentage along its length. 
            let currentPoint = line.getPoint(i / waves);

            // graphics.fillStyle(0xffff00).fillCircle(currentPoint.x, currentPoint.y, 4);

            //Defines a Line segment, a part of a line between two endpoints.
            let ray = new Phaser.Geom.Line(prevX, prevY, currentPoint.x, currentPoint.y);

            // graphics.lineStyle(1, 0xffffff).strokeLineShape(ray);

            //Calculate the normal of the given line.The normal of a line is a vector that points perpendicular from it.
            let normal = Phaser.Geom.Line.GetNormal(ray);
            //Get the midpoint of the given line.
            let midPoint = Phaser.Geom.Line.GetMidPoint(ray);

            // graphics.fillStyle(0x00ff00).fillCircle(midPoint.x + normal.x * vx, midPoint.y + normal.y * vy, 4);

            //Push to points array a representation of a vector in 2D space. A two-component vector.
            points.push(new Phaser.Math.Vector2(midPoint.x + normal.x * vx, midPoint.y + normal.y * vy));

            prevX = currentPoint.x;
            prevY = currentPoint.y;

            vx *= -1;
            vy *= -1;
        }

        //Push to points array, returns a Vector2 object that corresponds to the end of this Line.
        points.push(line.getPointB());
        //console.log(points);

        //Create a smooth 2d spline curve from a series of points.
        this.curve = new Phaser.Curves.Spline(points);

        graphics.lineStyle(1, 0xffffff, 1);
        this.curve.draw(graphics, 64);

        this.setFriction(0);
        this.setFrictionAir(0);
        this.setBounce(0);
    }
}
