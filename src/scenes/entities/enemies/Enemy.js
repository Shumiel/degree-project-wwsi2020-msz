//general Enemy class that defines basic behaviour
export default class Enemy extends Phaser.Physics.Matter.Sprite{
    constructor(data){         
        const {scene, x, y, texture, frame, fixedName, playerRef, gateRef} = data; //deconstruct received data
        super(scene.matter.world, x, y, texture, frame); //call super() in order to pass args to inherited Phaser.Physics.Matter.Sprite
        this.scene.add.existing(this); //adds an existing Game Object to this Scene

        // @ts-ignore
        const {Body, Bodies} = Phaser.Physics.Matter.Matter; //deconstruct Body and Bodies from Phaser.Physics.Matter.Matter
        let enemyCollider = Bodies.circle(this.x, this.y, 15, {isSensor:false, label:'enemyCollider', /*onCollideCallback: () =>{}*/}); //modify body collider 
        //let enemySensor = Bodies.circle(this.x, this.y, 30, {isSensor:true, label:'enemySensor', /*onCollideCallback: () =>{}*/}); //modify body    sensor  
        const compoundBody = Body.create({
            parts:[enemyCollider, /*enemySensor*/],
            frictionAir: 0.30,
        }); //compound custome body
        this.setExistingBody(compoundBody); //set new body to game object
        this.setFixedRotation(); //stops game object from rotation 360* when touched
        this.setBounce(1); //define restitution of body
        this.setMass(0.5); //affects setIntertia and setDensity

        /*START - Enemy characteristic*/
        //Stats
        this.enemyName = "Basic Enemy";
        this.enemyFixedName = fixedName; // I use it in order to correctly load animations for specific enemy
        this.enemyLife = 100;
        this.attackPower = 15;
        this.movementspeed = 1; 
        //Flags
        this.isAttacking = false;  
        this.isAlive = true;
        this.didPlayerAgro = false; //flag for following logic, I use it in order to determine if enemy should follow player only
        this.isTakingDamage = false;
        /* END  - Enemy characteristic*/
        this.scene = scene;
        this.playerRef = playerRef;
        this.gateRef = gateRef;
        
        this.hpRatio= 40/this.enemyLife;
        this.barHP = new Phaser.GameObjects.Graphics(this.scene);
        this.drawHpBar();
        this.scene.add.existing(this.barHP);
        
        this.create(); //call create() in constructor, because it won't do it alone here
    }

    preUpdate(time, delta){
        super.preUpdate(time, delta); //without this the animations get messed up, it's probably due to fact how phaser engine is constructed and need time in order to count frames
        //console.log("Enemy.js - update...")
        this.drawHpBar(); //draw life bar and update it every frame
        
        if(this.enemyLife > 0 && !this.isAttacking){
            this.anims.play(this.enemyFixedName + '_run',true);
            this.movesetLogic(this.playerRef, this.gateRef); //add logic stored in movesetLogic()
        } else if(this.enemyLife > 0 && this.isAttacking){
            this.anims.play(this.enemyFixedName + '_idle',true);
            //this.setTint(Math.random() * 0xffffff);
        }
        
        if(this.enemyLife <= 0) {
            this.killEnemy()
        }        
    }
 
    create(){
        console.log("Enemy.js - create...");
        //this.enemyInfo();
       
        this.setCollisionGroup(this.scene.nonCollidingGroup); 
    }   
    
    enemyInfo(){
        console.log(this);
    }

    movesetLogic(playerRef, gateRef){
        const speed = this.movementspeed;
        let enemyVelocity = new Phaser.Math.Vector2(); //create vector
        //determine what is closer - player or gate, then setVelocity in right direction
        ////try to test Math.abs() instead
        if (((Math.pow(playerRef.x - this.x, 2) + Math.pow(playerRef.y - this.y, 2) > Math.pow(gateRef.body.position.x - this.x, 2) + Math.pow(gateRef.body.position.y - this.y, 2)) || playerRef.playerLife <= 0) && !this.didPlayerAgro)
        {
            //Follow doors logic           
            //logic for modyfing vector x,y
            if(gateRef.body.position.x > this.x){
                enemyVelocity.x = 1;  
            } else if(gateRef.body.position.x < this.x) { enemyVelocity.x = -1}

            if(gateRef.body.position.y > this.y){
                enemyVelocity.y = 1;  
            } else if(gateRef.body.position.y < this.y) { enemyVelocity.y = -1}
        } else if (playerRef.playerLife > 0) {
            //Follow the player logic          
            //logic for modyfing vector x,y
            if(playerRef.x > this.x){
              enemyVelocity.x = 1;  
            } else if(playerRef.x < this.x) { enemyVelocity.x = -1}
        
            if(playerRef.y > this.y){
              enemyVelocity.y = 1;  
            } else if(playerRef.y < this.y) { enemyVelocity.y = -1}         
        }
        
        enemyVelocity.normalize().scale(speed); //keeps magnitude as 1 (stable velocity) and scale it with speed variable
        this.setVelocity(enemyVelocity.x, enemyVelocity.y); //set velocity of physic body
    }

    bounceBackOnCollisionWithPlayer(playerRef){
        this.isAttacking = true;
        playerRef.isNotDamaged = false;   
        this.didPlayerAgro = true;     

        //calculate player bounce
        const dx = playerRef.x - this.x
		const dy = playerRef.y - this.y                
        const dir = new Phaser.Math.Vector2(dx, dy).normalize().scale(10)
        playerRef.setVelocity(dir.x, dir.y)     
       
        //calculate enemy bounce
        const dxMonster = this.x - playerRef.x
        const dyMonster = this.y - playerRef.y
        const dirMonster = new Phaser.Math.Vector2(dxMonster, dyMonster).normalize().scale(5)                 
        this.setVelocity(dirMonster.x, dirMonster.y)
        
        setTimeout(()=>{
            this.isAttacking = false;
            playerRef.isNotDamaged = true;
        }, 250);     
    }

    bounceBackOnCollisionWithGate(gateRef){
        this.isAttacking = true;       
       
        //calculate enemy bounce
        const dxMonster = this.x - gateRef.body.position.x
        const dyMonster = this.y - gateRef.body.position.y
        const dirMonster = new Phaser.Math.Vector2(dxMonster, dyMonster).normalize().scale(15)                 
        this.setVelocity(dirMonster.x, dirMonster.y)
        
        setTimeout(()=>{
            this.isAttacking = false;
        }, 250);     
    }

    bounceBackCollisionWithPlayerMeleeSensorAttack(sensorRef){
        this.isAttacking = true;       
        this.didPlayerAgro = true;
       
        //calculate enemy bounce
        const dxMonster = this.x - sensorRef.position.x
        const dyMonster = this.y - sensorRef.position.y
        const dirMonster = new Phaser.Math.Vector2(dxMonster, dyMonster).normalize().scale(15)                 
        this.setVelocity(dirMonster.x, dirMonster.y)
        
        setTimeout(()=>{
            this.isAttacking = false;
        }, 250);   
    }

    decreaseEnemyLife (amount, dmg) //dmg - damage type
    {
        this.isTakingDamage = true;
        //there is no better place at this moment to locate that flag change on arrow hit to monster
        if(dmg === "arrow"){
            this.didPlayerAgro = true;
        }

        //common logic for decreasing hp
        this.enemyLife -= amount;

        if (this.enemyLife < 0){
            this.enemyLife = 0;
        }

        this.drawHpBar();

        this.scene.time.delayedCall(3000, () => {
            this.isTakingDamage = false;
        });
        
        return (this.enemyLife === 0);
    }

    drawHpBar ()
    {
        if(this.enemyLife > 0){
            this.barHP.clear();
            //barHP background
            this.barHP.fillStyle(0x000000);
            this.barHP.fillRect(this.x - 20, this.y + 25, 40, 1);
    
            //barHP health
            this.barHP.fillStyle(0xffffff);
            this.barHP.fillRect(this.x - 20, this.y + 25, 40, 1);
            
            if (this.enemyLife < 30) {
                this.barHP.fillStyle(0xff0000);
            }
            else {
                this.barHP.fillStyle(0x00ff00);
            }
    
            let d = Math.floor(this.hpRatio * this.enemyLife);
            this.barHP.fillRect(this.x - 20, this.y + 25, d, 1);
        } else {
            this.barHP.clear();
        }
    }

    killEnemy(){
        if(this.isAlive){
            this.isAlive = false;

            this.barHP.destroy();

            this.setTint(Math.random() * 0xffffff);
            this.setCollisionCategory(null);

            this.determineGoldForMonster()

            setTimeout(() => {
                // @ts-ignore
                // modify referenced property in MainScene for WaveCleared() method logic
                this.scene.leftEnemies -= 1;
                
                this.destroy();                
            }, 300);        
        }       
    }


    determineGoldForMonster(){    
        let randomDrop = Phaser.Math.Between(5,25);    
        this.playerRef.gold += randomDrop;
    }
}
