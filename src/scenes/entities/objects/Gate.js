export default class Gate{
    constructor(data){
        let { x, y, width, height, isStatic, scene } = data;
        this.scene = scene;
        this.body = this.scene.matter.add.rectangle(x, y, width, height, { isStatic });
        this.body.label = "Gate"; //need to determine label for easier collision detection in MainScene()

        this.gateLife = 1000;

        this.hpRatio= 30/this.gateLife;
        this.barHP = new Phaser.GameObjects.Graphics(this.scene);
        this.scene.add.existing(this.barHP);
        this.drawHpBar();

        this.open = this.scene.add.sprite(320,32, 'gate', 0);
        this.closed = this.scene.add.sprite(320,32, 'gate', 1);

        this.create();
    }    
    
    create(){
        this.body;
    }

    update(){
        this.drawHpBar(); //draw life bar and update it every frame

        if(this.gateLife <= 0){
            this.closed.destroy();
            //this.open;
        }
    }

    decreaseGateLife (amount)
    {
        this.gateLife -= amount;

        if (this.gateLife < 0){
            this.gateLife = 0;
        }

        this.drawHpBar();
        
        return (this.gateLife === 0);
    }

    drawHpBar ()
    {
        if(this.gateLife > 0){
            this.barHP.clear();            
        //barHP background
            this.barHP.fillStyle(0x000000);
            this.barHP.fillRect(this.body.position.x - 15, this.body.position.y - 22, 30, 3);
    
        //barHP health
            this.barHP.fillStyle(0xffffff);
            this.barHP.fillRect(this.body.position.x - 15, this.body.position.y - 22, 30, 3);
             
            if (this.gateLife < 300) {
                this.barHP.fillStyle(0xff0000);
            }
            else {
                this.barHP.fillStyle(0x00ff00);
            }
 
            let d = Math.floor(this.hpRatio * this.gateLife);
            this.barHP.fillRect(this.body.position.x - 15, this.body.position.y - 22, d, 3);
        } else {
            this.barHP.clear();
        }
    }
}