//prototype of arrow class - need to be removed or refactored later
//at this point I do not use it in project
export default class MagicProjectile extends Phaser.Physics.Matter.Sprite{
    constructor(data){
        let { x, y, texture, labelName, scene, playerRef, gateRef, enemyRef } = data;
        super(scene.matter.world, x, y, undefined, undefined); //call super() in order to pass args to inherited Phaser.Physics.Matter.Sprite

        //modify magic projectile collision figure and properties
        // @ts-ignore
        const {Body, Bodies} = Phaser.Physics.Matter.Matter;
        let magicProjectileCollider = Bodies.circle(this.x, this.y, 10, {isSensor:true, label:labelName+"Collider", onCollideCallback: (event) =>{
            //console.log(event);
            if(event.bodyB.label === "MagicProjectileCollider" && (event.bodyA.label === "playerCollider" || event.bodyA.label === "Gate")){
                if(event.bodyA.label === "playerCollider"){
                    this.playerRef.decreasePlayerLife(enemyRef.attackPower);
                    this.emitter.explode(500, playerRef.x, playerRef.y);
                }

                if(event.bodyA.label === "Gate"){
                    this.gateRef.decreaseGateLife(enemyRef.attackPower);
                    this.emitter.explode(500, gateRef.x, gateRef.y);
                }

                this.emitter.stopFollow(this);
                
                //this.emitter.killAll();
                this.destroy();
            }
        }});
        let magicProjectileSensor = Bodies.circle(this.x, this.y, 50, {isSensor:true, label:labelName+"Sensor", onCollideCallback: () =>{
            //do stuff when sensor touched ......
            // .....................
        }});
        const compoundBody = Body.create({
            parts:[magicProjectileCollider, magicProjectileSensor]
        });
        this.setExistingBody(compoundBody); //apply new changes to the body

        this.scene.add.existing(this); //adds an existing Game Object to this Scene

        //assign deconstructed data
        this.x = x;
        this.y = y;
        this.texture = texture;
        this.labelName = labelName;
        this.scene = scene;
        this.playerRef = playerRef;
        this.gateRef = gateRef;
        this.enemyRef = enemyRef;
        
        //stats
        this.speed = 2.5;

        //configuration
        // this.setBody({
        //     type: 'circle',
        //     radius: 5,
        // });
        //just make the body move around and bounce
        this.setAngularVelocity(0.01);
        this.setBounce(1);
        this.setFriction(0, 0, 0);

        
       


        //call create() - otherwise without constructor it won't call itself when created in other scenes
        this.create();
    }
    
    
    create(){      
        let particles = this.scene.add.particles('necromancer_magic');
        const particleSpeed = 40;
        this.emitter = particles.createEmitter({
           
            speed: {
                onEmit: function (particle, key, t, value)
                {
                    return 50;
                }
            },
            lifespan: {
                onEmit: function (particle, key, t, value)
                {
                    return Phaser.Math.Percent(10, 0, 300) * 20000;
                }
            },
            alpha: {
                onEmit: function (particle, key, t, value)
                {
                    return Phaser.Math.Percent(15, 0, 300) * 1000;
                }
            },
            scale: { start: 2.0, end: 0 },
            blendMode: 'ADD'
        });
        
        this.emitter.startFollow(this);
    }

    preUpdate(time, delta){
        super.preUpdate(time, delta);

        const speed = this.speed;
        let magicVelocity = new Phaser.Math.Vector2(); //create vector
        //Follow the player logic          
        //logic for modyfing vector x,y
        if (((Math.pow(this.playerRef.x - this.x, 2) + Math.pow(this.playerRef.y - this.y, 2) > Math.pow(this.gateRef.body.position.x - this.x, 2) + Math.pow(this.gateRef.body.position.y - this.y, 2)) || this.playerRef.playerLife <= 0) && !this.enemyRef.didPlayerAgro)
        {
            //Follow doors logic           
            //logic for modyfing vector x,y
            if(this.gateRef.body.position.x > this.x){
                magicVelocity.x = 1;  
            } else if(this.gateRef.body.position.x < this.x) { magicVelocity.x = -1}
              if(this.gateRef.body.position.y > this.y){
                magicVelocity.y = 1;  
            } else if(this.gateRef.body.position.y < this.y) { magicVelocity.y = -1}
        } else if (this.playerRef.playerLife > 0) {
            //Follow the player logic          
            //logic for modyfing vector x,y
            if(this.playerRef.x > this.x){
              magicVelocity.x = 1;  
            } else if(this.playerRef.x < this.x) { magicVelocity.x = -1}
        
            if(this.playerRef.y > this.y){
              magicVelocity.y = 1;  
            } else if(this.playerRef.y < this.y) { magicVelocity.y = -1}         
        }
       
        magicVelocity.normalize().scale(speed); //keeps magnitude as 1 (stable velocity) and scale it with speed variable
        this.setVelocity(magicVelocity.x, magicVelocity.y); //set velocity of physic body    
    }

}