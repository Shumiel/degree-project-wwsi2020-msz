//prototype of arrow class - need to be removed or refactored later
//at this point I do not use it in project
export default class Fog extends Phaser.Physics.Matter.Sprite{
    constructor(data){
        let { x, y, labelName, scene, particleSpeed, particleAlpha, particleLifespan, texture, size} = data;
        super(scene.matter.world, x, y, undefined, undefined); //call super() in order to pass args to inherited Phaser.Physics.Matter.Sprite

       
        //modify magic projectile collision figure and properties
        // @ts-ignore
        const {Body, Bodies} = Phaser.Physics.Matter.Matter;
        let FogCollider = Bodies.circle(this.x, this.y, 10, {isSensor:true, label:labelName+"Collider", onCollideCallback: (event) =>{
            //console.log(event);
          
        }});
        let FogSensor = Bodies.circle(this.x, this.y, 50, {isSensor:true, label:labelName+"Sensor", onCollideCallback: (event) =>{
            //do stuff when sensor touched ......
            // .....................
        }});
        const compoundBody = Body.create({
            parts:[FogCollider, FogSensor]
        });
        this.setExistingBody(compoundBody); //apply new changes to the body

        this.scene.add.existing(this); //adds an existing Game Object to this Scene

        //assign deconstructed data
        this.x = x;
        this.y = y;
        this.labelName = labelName;
        this.scene = scene;
        this.particleSpeed = particleSpeed;
        this.particleAlpha = particleAlpha;
        this.particleLifespan = particleLifespan;
        this.texture = texture;
        this.size = size;

        //just make the body move around and bounce
        this.setAngularVelocity(0.01);
        this.setBounce(1);
        this.setFriction(0, 0, 0);

        //call create() - otherwise without constructor it won't call itself when created in other scenes
        this.create();
    }
    
    
    create(){      
        this.particles = this.scene.add.particles(this.texture);
        const    particleSpeed = this.particleSpeed;
        const particleLifespan = this.particleLifespan;
        const    particleAlpha = this.particleAlpha;
        this.emitter = this.particles.createEmitter({
           
            speed: {
                onEmit: function (particle, key, t, value)
                {
                    return particleSpeed;
                }
            },
            lifespan: {
                onEmit: function (particle, key, t, value)
                {
                    return Phaser.Math.Percent(particleLifespan, 0, 300) * 20000;
                }
            },
            alpha: {
                onEmit: function (particle, key, t, value)
                {
                    return Phaser.Math.Percent(particleAlpha, 0, 300) * 1000;
                }
            },
            scale: { start: 0.1, end: this.size },
            blendMode: 'ADD'
        });
        
        this.emitter.startFollow(this);
    }

    preUpdate(time, delta){
        super.preUpdate(time, delta);

    }

}