/*
    player class is responsible for creating basic logic for player ingame 
*/
export default class Player extends Phaser.Physics.Matter.Sprite{
    constructor(data){
        let {scene, x, y, texture, frame, playerInput} = data; 
        super(scene.matter.world, x, y, texture, frame);
        this.scene.add.existing(this);

        this.create(playerInput); //call create() method to create inputs for player

        //modify player collision figure and properties
        // @ts-ignore
        const {Body, Bodies} = Phaser.Physics.Matter.Matter;
        var playerCollider = Bodies.circle(this.x, this.y, 15, {isSensor:false, label:'playerCollider', /*onCollideCallback: () =>{console.log('playerCollider')}*/});
        //var playerSensor = Bodies.circle(this.x, this.y, 50, {isSensor:true, label:'playerSensor'});
        const compoundBody = Body.create({
            parts:[playerCollider, /*playerSensor*/],
            frictionAir: 0.30,
            restitution: 10,
        });
        this.setExistingBody(compoundBody); //apply new changes to the body
        this.setFixedRotation(); //prevents from rotating the object while colliding with another
        this.setBounce(1); 
        this.setMass(0.9);
        this.setDepth(1);


        /*START - Player characteristic*/
        //Stats
        this.playerLife = 100;
        this.attackPower = 25;
        this.arrowPower = 50;
        this.magicPower = 90;
        this.arrows = 3;
        this.gold = 0;
        this.playerVelocityX = 0;
        this.playerVelocityY = 0;
        //Flags
        this.isNotDamaged = true; //need it to determine if player took damage from other enemies - I set it player as 
        this.isAttacking = false;
        this.directionFlag = ""; //left or right, need it for bow attacks in order to determine where arrow should appear while player is iddle and is trying to attack
        this.isGamePaused = false;
        /*END - Player characteristic*/

        
        //optional properties
        this.p= 40/100; //ratio for drawing HP bar
        this.barHP = new Phaser.GameObjects.Graphics(this.scene); //HP bar object
        this.barHP.setDepth(1);
        this.drawHpBar(); //function to draw HP bar
        this.scene.add.existing(this.barHP); //
    }

    /*
    phaser create() method - called at the start of object creation, after constructor
    */
    create(playerInput){
        console.log('Player.js - create...')

        //define available inputs for player
        this.inputKeys = playerInput.keyboard.addKeys({
            up: Phaser.Input.Keyboard.KeyCodes.W,
            down: Phaser.Input.Keyboard.KeyCodes.S,
            left: Phaser.Input.Keyboard.KeyCodes.A,
            right: Phaser.Input.Keyboard.KeyCodes.D,
            space: Phaser.Input.Keyboard.KeyCodes.SPACE,
            b: Phaser.Input.Keyboard.KeyCodes.B,
            p: Phaser.Input.Keyboard.KeyCodes.P,
        });  
    }

    /*
    without this getter we can't access velocity
    */
    get velocity(){
        return this.body.velocity; //this.velocity exists on body so we need to call it 
    }
  
    /*
    phaser update() method - called every frame refresh, infinite loop
    */
    update(){
        if(this.playerLife > 0){            
            //logic for HP bar drawing
            if(this.playerLife > 0 && !this.isNotDamaged || this.isAttacking){ 
                this.drawHpBar();
            }
    
            //call logic for melee attack
            this.meleeAttack();

            //call logic for bow attack
            this.bowAttack();

            //call logic for moving
            this.movePlayer();

            //call logic for pausing the game
            this.pauseGame();
        } else {
            this.killPlayer();
        }
    }

   /*
       method responsible for modyfing playerLife, responsible for decrasing player life during collisions with other objects
       @parm {number} amount - number of life to be substracted from player life
   */
   decreasePlayerLife (amount)
   {
       //decrease player life by amount
       this.playerLife -= amount;

       //set player life to 0 if dead in order to keep other logics fine
       if (this.playerLife <= 0){
           this.playerLife = 0;
       }

       //update HP bar state
       this.drawHpBar();
       //player appropriate animation
       this.anims.play("hero_hurt", true)        
       return (this.playerLife === 0);
   }

   /*
       method responsible for drawing life bar of player, should be called during frame updates or for specific events
   */
   drawHpBar ()
   {
       //clear bar HP canvas, re-draw it
       this.barHP.clear();
       //barHP background
       this.barHP.fillStyle(0x000000);
       this.barHP.fillRect(this.x - 20, this.y - 20, 40, 1);

       //barHP health
       this.barHP.fillStyle(0xffffff);
       this.barHP.fillRect(this.x - 20, this.y - 20, 40, 1);
       
       //logic for coloring HP bar
       if (this.playerLife < 30) {
           this.barHP.fillStyle(0xff0000);
       }
       else {
           this.barHP.fillStyle(0x00ff00);
       }

       //calculate width proportion dynamicly and apply it to fillRect() method
       let d = Math.floor(this.p * this.playerLife);
       this.barHP.fillRect(this.x - 20, this.y - 20, d, 1);
   }

   /*
   old attack function, prototype - just an idea
   */
   attack(){
       if(this.inputKeys.space.isDown && !this.isAttacking && (this.playerLife > 0) && (this.playerVelocityX != 0 || this.playerVelocityY != 0)){
           this.isAttacking = true;
           let availableAnims = ["hero_attack", "hero_attack2", "hero_attack3"];
           let randomAnim = Phaser.Math.Between(0,2);
           this.anims.play(availableAnims[randomAnim], true);
           
           //trying to figure out how to position shape in the correct direction
           let fixX = this.x + this.playerVelocityX;
           let fixY = this.y + this.playerVelocityY;
           let widthArea = 10;
           let heightArea = 30;

           if(this.playerVelocityX < 0){
               fixX -= 25;
           } else if( this.playerVelocityX > 0){
               fixX += 25;
           }

           if(this.playerVelocityY < 0){
               fixY -= 25;
               widthArea = 30;
               heightArea = 10;
           } else if(this.playerVelocityY > 0){
               fixY += 25;
               widthArea = 30;
               heightArea = 10;
           }
           
           
           let attackMeleeArea = this.scene.matter.add.rectangle(fixX, fixY, widthArea, heightArea, { 
               chamfer: { radius: [5, 5, 5, 5] },
               isStatic: true,
             });
           attackMeleeArea.label = "attackMeleeArea";

           // let attackMeleeArea = this.scene.matter.add.polygon(fixX, fixY, 4, 5, { 
           //     chamfer: { radius: [-10, 30, -10] }
           //   });
           // 

           setTimeout(()=>{ 
               this.isAttacking = false;
               this.scene.matter.world.remove(attackMeleeArea);
           }, 500);
       }
   }

   /*
   main melee attack function
   */
   meleeAttack(){
       if(this.inputKeys.space.isDown && !this.isAttacking){
           //modify flag for futher logic
           this.isAttacking = !this.isAttacking;
           
           this.playerVelocityX = 0;
           this.playerVelocityY = 0;

           //play one of three available animation, proces is random for each attack
           let availableAnims = ["hero_attack", "hero_attack2", "hero_attack3"];
           let randomAnim = Phaser.Math.Between(0,2);
           this.anims.play(availableAnims[randomAnim], true);
           
           //add sensor to the world
           let meleeSensor = this.scene.matter.add.circle(this.x, this.y, 25, {isSensor: true, isStatic: false, label: "meleeSensor"});
           
           //after short period remove sensor and unlock action again
           setTimeout(() => {
               this.scene.matter.world.remove(meleeSensor);
               this.isAttacking = !this.isAttacking;
           }, 400);
       }
   }

   /*
   bow attack function
   */
   bowAttack(){
       if(this.inputKeys.b.isDown && !this.isAttacking && this.arrows > 0){
        this.isAttacking = !this.isAttacking;
        this.arrows -= 1;
        //play one of three available animation, proces is random for each attack
        this.anims.play("hero_bow", true); 
        
        //calculate direction where arrow should appear during idle/move of the player
        let projectileVelocity = new Phaser.Math.Vector2();
        let fixX = this.x;
        let flipImage = false;
        if(this.directionFlag == "left"){
            fixX -= 25;
            projectileVelocity.x = -1;
            flipImage = true;
        } else if( this.directionFlag == "right" || this.directionFlag == "top" || this.directionFlag == "bottom"){
            fixX += 25;
            projectileVelocity.x = 1;
        } else if (this.directionFlag == ""){
            fixX += 25;
            projectileVelocity.x = 1;
        }   
        
        //normalize values in order to prevent speeding up while moving
        projectileVelocity.normalize();
        //scale movement speed with speed variable
        projectileVelocity.scale(10);
        
        //change flag back to allow attack again and do other stuff after a period of time
        setTimeout(() => {
            this.isAttacking = !this.isAttacking;
            //create arrow object
            let arrow = this.scene.matter.add.image(fixX, this.y, 'arrow', undefined, {label:"arrow", onCollideCallback: (event)=>{
                //
            }});
            
            //change display size of arrow
            arrow.setDisplaySize(25,5);

            //if player is facing towards left flip arrow image
            if(flipImage){
                arrow.flipX = flipImage;
            }
            //apply current velocity of the object
            arrow.setVelocity(projectileVelocity.x, projectileVelocity.y);

            arrow.setOnCollideActive((event) => {
                console.log(event);
            });
        }, 700);
       }
   }

   /*
   main player moveset logic
   */
   movePlayer(){
       //let player move only if alive, if player is not damaged by other objects, if player is not attacking
       if(this.playerLife > 0 && this.isNotDamaged && !this.isAttacking){
           //draw HP bar along player while moving
           this.drawHpBar();
           //define player speed (we scale this value with object move speed)
           const speed = 2.5;
           //create new vector 2d
           let playerVelocity = new Phaser.Math.Vector2();
           //determine x and y for vector
           if(this.inputKeys.left.isDown){
               playerVelocity.x = -1;
               this.directionFlag = "left";
           } else if(this.inputKeys.right.isDown){
               playerVelocity.x = 1;
               this.directionFlag = "right";
           }
           if(this.inputKeys.up.isDown){
               playerVelocity.y = -1;
               this.directionFlag = "top";
           } else if(this.inputKeys.down.isDown){
               playerVelocity.y = 1;
               this.directionFlag = "bottom";
           }
           //normalize values in order to prevent speeding up while moving
           playerVelocity.normalize();
           //scale movement speed with speed variable
           playerVelocity.scale(speed);
           //set x,y velocity to separate properties of the constructor in order to use it for other logics
           this.playerVelocityX = playerVelocity.x;
           this.playerVelocityY = playerVelocity.y;
           //apply current velocity of the object
           this.setVelocity(playerVelocity.x, playerVelocity.y);
       
           //logic for changing animation according to velocity direction or if no action is taken
           if(Math.abs(this.velocity.x) > 0.1 || Math.abs(this.velocity.y) >0.1){
               if(this.inputKeys.left.isDown){
                   this.anims.play('hero_run',true);
                   this.flipX=true;
               }else {
                   this.anims.play('hero_run',true);
                   this.flipX=false;
               }
           } else { this.anims.play('hero_idle',true); }
        }
    }

    /*
    method responsible for killing the player
    */
    killPlayer(){
        //destory HP bar
        this.barHP.destroy();
        //if player life is equal 0, proceed with appropriate animation and interrupt the loop with life equal to -1
        if(this.playerLife === 0){
            this.anims.play('hero_die', true);
            this.playerLife = -1; //it will occur only once in update() loop cycle, without it hero die every time when frame refresh
        }
    }

    /**
     * method called when player wants to pause the game
     */
    pauseGame(){
        //I call here reference, so instead this.scene.pause() we have this.scene.scene.pause()
        if(!this.isGamePaused && this.inputKeys.p.isDown){
            this.isGamePaused = true;
            // this.scene.time.delayedCall(1000, () => {
            //    this.scene.scene.pause();
            // });  
            console.log(this.isGamePaused);
        } else if (this.isGamePaused && this.inputKeys.p.isDown){
            //this.scene.scene.resume();
            this.isGamePaused = false;
            console.log(this.isGamePaused);
        }
    }
}