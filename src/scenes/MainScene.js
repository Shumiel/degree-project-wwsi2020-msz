import Player from "./entities/Player.js";
import Enemy from "./entities/enemies/Enemy.js";
import Necromancer from "./entities/enemies/Necromancer.js";
import {mapCreate} from "./Map.js";
import Gate from "./entities/objects/Gate.js";

export default class MainScene extends Phaser.Scene {
    constructor(){
        super('MainScene');

        this.isGameOver = false;

        this.currentLevel = 1;
        this.leftEnemies = 5;
    }

    create(){
        console.log("MainScene - create...")
       
        //keep flag clean so every scene restart due to player failed mission, game can restart again and again, without this we get only one restart from gameOver() method
        this.isGameOver = false;
        
        //call code responsible for map creation and collisions
        mapCreate.call(this);
       
        //add players to the scene
        this.player = new Player({scene: this, x:320, y:80, texture: 'hero', frame: 'adventurer-idle-03', playerInput: this.input});
        //add info about player arrow to view
        this.arrowsText = this.add.text(0, 0, 'Arrows:', {fontSize: 15});
        //add info about player arrow to view
        this.goldText = this.add.text(90, 0, 'Gold:', {fontSize: 15});
        //add gate to the scene
        this.gate = new Gate({ scene: this, x: 320, y: 30, width: 30, height: 12, isStatic: true });
        
        //create non-colliding group    
        this.nonCollidingGroup = this.matter.world.nextGroup(true); 
        
        //create monster and bound non-colliding group with it
        this.time.delayedCall(1000, ()=>{
            this.enemy = new Enemy({scene: this, x:150, y:250, texture: 'big_demon', frame: 'big_demon_idle_anim_f0', fixedName: 'bdemon', playerRef: this.player, gateRef: this.gate});
            
        });        
        this.time.delayedCall(2500, ()=>{
            this.enemy2 = new Enemy({scene: this, x:320, y:300, texture: 'big_demon', frame: 'big_demon_idle_anim_f0', fixedName: 'bdemon', playerRef: this.player, gateRef: this.gate});
               
        });        
        this.time.delayedCall(4000, ()=>{
            this.enemy3 = new Enemy({scene: this, x:490, y:250, texture: 'big_demon', frame: 'big_demon_idle_anim_f0', fixedName: 'bdemon', playerRef: this.player, gateRef: this.gate});
            
        });
        //create necromancers
        this.time.delayedCall(2000, ()=>{
            this.enemy4 = new Necromancer({scene: this, x:320, y:300, texture: 'necromancer', frame: 'necromancer_idle_anim_f0', fixedName: 'necromancer', playerRef: this.player, gateRef: this.gate, side: "left"});              
           
        });        
        this.time.delayedCall(5000, ()=>{
            this.enemy5 = new Necromancer({scene: this, x:320, y:300, texture: 'necromancer', frame: 'necromancer_idle_anim_f0', fixedName: 'necromancer', playerRef: this.player, gateRef: this.gate, side: "right"});              
            
        });
        
        


        //collisionstart logic
        this.matter.world.on("collisionstart", event => {
            event.pairs.forEach(pair => {
              const { bodyA, bodyB } = pair; //destruct pair and take out bodyA and bodyB from it
                          
              const aIsPlayer = bodyA.gameObject instanceof Player; //boolean, check if bodyA is a Player
              const aIsGate = (bodyA.label == "Gate"); //boolean, check if bodyA is a Gate
              const aIsMeleeSensor = (bodyA.label === "meleeSensor"); //boolean, check if bodyA is a player meele attack
              const bIsMeeleSensor = (bodyB.label === "meleeSensor"); //boolean, check if bodyB is a player meele attack
              const aIsEnemy = bodyA.gameObject instanceof Enemy; //booelan, check if bodyB is enemy|monster
              const bIsEnemy = bodyB.gameObject instanceof Enemy; //booelan, check if bodyB is enemy|monster
              const aIsArrow = (bodyA.label == "arrow") //booelan, check if bodyB is arrow              
              const bIsArrow = (bodyB.label == "arrow"); //booelan, check if bodyB is arrow
              
              //logic for collision between gate and enemies  
              if(aIsGate && bIsEnemy){            
                console.log("Gate attacked!");
                bodyB.gameObject.bounceBackOnCollisionWithGate(this.gate); //I use here bodyB.gameObject, thanks to that every enemy will work with that part of code
                this.gate.decreaseGateLife(bodyB.gameObject.attackPower); //I have only one gate so I call here this.gate instead bodyA
              }

              //logic for collision between player and enemies    
              if(aIsPlayer && bIsEnemy){
                //console.log("Player attacked!");
                bodyB.gameObject.bounceBackOnCollisionWithPlayer(this.player); 
                this.player.decreasePlayerLife(bodyB.gameObject.attackPower);               
              }
              
              //logic for collision between player melee attack and enemies
              if((aIsMeleeSensor && bIsEnemy) || (aIsEnemy && bIsMeeleSensor)){
                if((aIsMeleeSensor && bIsEnemy)){
                    bodyB.gameObject.bounceBackCollisionWithPlayerMeleeSensorAttack(bodyA);
                    bodyB.gameObject.decreaseEnemyLife(this.player.attackPower);
                } else if((aIsEnemy && bIsMeeleSensor)){
                    bodyA.gameObject.bounceBackCollisionWithPlayerMeleeSensorAttack(bodyB);
                    bodyA.gameObject.decreaseEnemyLife(this.player.attackPower);
                }
              }
              
              //logic for arrows collision
              if(aIsArrow || bIsArrow){
                console.log(bodyA, bodyB);
                // if(aIsArrow){
                //     this.matter.world.remove(bodyA); 
                //     setTimeout(() => {
                //         bodyA.gameObject.destroy(); 
                //     }, 50);
                // }
                
                if(bodyA.label == "Rectangle Body" && bIsArrow){
                    this.matter.world.remove(bodyB);
                    setTimeout(() => {
                        bodyB.gameObject.destroy(); 
                    }, 50);   
                }
                 
                if(aIsEnemy && bIsArrow){
                    bodyA.gameObject.decreaseEnemyLife(this.player.arrowPower, "arrow"); 
                    //this.matter.world.remove(bodyB.gameObject);      
                    bodyB.gameObject.destroy();         
                }
              }

             });
            });

        //collisionactive
        //...
        //collisionend
        //...
    }
    
    update(){
        //call other gameObjects updates
        this.player.update();
        this.gate.update();
        
        //logic for player or gate being destroyed
        this.gameOver();

        //logic what to do if wave is cleared
        this.waveCleared();

        //show available arrows and gold for player
        this.showPlayerArrows();
        this.showPlayerGold()
    }
    
    /*
        method called when player lose the game
    */
    gameOver(){
        //end the game if player or gate will be destroyed, change flag in order to prevent loop
        if((this.player.playerLife <= 0 || this.gate.gateLife <= 0) && !this.isGameOver){
            //change flag
            this.isGameOver = true;
            //add image of skull
            this.add.image(320, 150,'skull');
            //create text
            let gameOverText = this.add.bitmapText(320, 240, 'game_font', 'GAME OVER').setOrigin(0.5, 0.5).setScale(1);
            //change text color to black
            gameOverText.tint = 0x0000;
            //fade out effects at the end, game will restart after 3050ms
            this.time.delayedCall(2000, () => {
                this.cameras.main.fadeOut(500);
		    	this.time.delayedCall(550, ()=>{
                    this.scene.restart();
                });
            })        
            // //pause the game
            // this.scene.pause();
        }
    }
    
    /**
     * method called when player beats all enemies
     */
    waveCleared(){
        if(this.leftEnemies == 0){
            this.leftEnemies = -1; // need to remove it, for now it prevents from loop
            //create text
            let waveClearedText = this.add.bitmapText(320, 240, 'game_font', 'Wave '+this.currentLevel+' cleared..').setOrigin(0.5, 0.5).setScale(1);
            waveClearedText.tint = 0x0000;
            //clear text after short period
            this.time.delayedCall(3000, () =>{
                waveClearedText.destroy();
                this.scene.restart(); //restart due to missing logic when new level/wave is reached
            });
        }
    }

    /**
     * method called in order to show left player arrows
     */
    showPlayerArrows(){
        if(typeof this.player.arrows !== 'undefined'){            
            this.arrowsText.setText(`Arrows:${this.player.arrows}`);
        }
    }

    /**
     * method called in order to show left player arrows
     */
    showPlayerGold(){
        if(typeof this.player.gold !== 'undefined'){            
            this.goldText.setText(`Gold:${this.player.gold}`);
        }
    }


}