import Phaser from 'phaser'

import MainScene from './scenes/MainScene';
import PreloadScene from './scenes/PreloadScene';

const config = {
	type: Phaser.AUTO,
	autoCenter: true,
	width: 640,
	height: 360,
	physics: {
		default: 'matter',
		matter: {
			// debug: {
			// 	showCollisions: true,
            //     collisionColor: 0xf5950c,
			// },	
			gravity: { y: 0 }
		},		
	},
	zoom: 2,
  	pixelArt: true,
	scene: [PreloadScene, MainScene],
	scale: {
		mode: Phaser.Scale.FIT
	}
}

export default new Phaser.Game(config)